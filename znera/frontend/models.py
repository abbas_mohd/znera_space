import sys
from django.db import models
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile

# Create your models here.

class ProjectType(models.Model):

    name = models.CharField(max_length=30, unique=True, help_text="")

    def __str__(self):
        return self.name


class Project(models.Model):

    name = models.CharField(max_length=150)
    project_type = models.ForeignKey(ProjectType, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.name}'


class ProjectImage(models.Model):

    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    default = models.BooleanField(default=False)
    image_width = models.PositiveIntegerField(blank=True)
    image_height = models.PositiveIntegerField(blank=True)
    image = models.ImageField(upload_to='projectImages/', height_field='image_height', width_field='image_width')

    __original_image = None
    
    def __init__(self, *args, **kwargs):
        super(ProjectImage, self).__init__(*args, **kwargs)
        self.__original_image = self.image

    def __str__(self):
        return f'{self.project} - {self.image.name.replace(self.image.field.upload_to, "")}'

    def clean(self):
        # A Project can have only one Image as default
        # If an ProjectImage is set as default when saving, 
        # any previous ProjectImage that was set as default will be overwritten
        if self.default and self.__class__.objects.filter(project=self.project, default=True).exists():
            default_record = self.__class__.objects.get(project=self.project, default=True)
            default_record.default = False
            default_record.save()
        if self.image != self.__original_image:
            self.image = self.compressImage(self.image)
        # if not self.__class__.objects.filter(project=self.project, default=True).exists():
        #     self.default = True
    
    def compressImage(self, uploadedImage):
        imageTemproary = Image.open(uploadedImage)
        outputIoStream = BytesIO()
        # imageTemproaryResized = imageTemproary.resize( (1020,573) ) 
        imageTemproary.save(outputIoStream , format='JPEG', optimize=True)
        outputIoStream.seek(0)
        uploadedImage = InMemoryUploadedFile(outputIoStream,'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0], 'image/jpeg', sys.getsizeof(outputIoStream), None)
        return uploadedImage
