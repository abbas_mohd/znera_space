from django.contrib import admin
from .models import ProjectType, Project, ProjectImage

# admin_site = MyAdminSite()

# Register your models here.

class ProjectImageAdmin(admin.ModelAdmin):
    exclude = ('image_height', 'image_width',)
    list_display = ('project', 'project_type', 'default', 'image')
    list_filter = ('project__project_type', 'project',)

    @admin.display(description='Project Type')
    def project_type(self, obj):
        return "%s" % (obj.project.project_type)

admin.site.register(ProjectType)
admin.site.register(Project)
admin.site.register(ProjectImage, ProjectImageAdmin)