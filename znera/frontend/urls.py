from django.urls import path
from .views import index

urlpatterns = [
    path('', index),
    path('projects/<slug:slug>/', index),
    path('about/', index),
]