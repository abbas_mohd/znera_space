import React, { useState } from 'react';
import { useSpring, animated} from "react-spring";
import ProjectDropdown from "./ProjectDropdown";
import Masonry from "./Masonry";
import Logo from "./Logo";

export default React.memo(function Landing(props) {

  // props.setShowCloseIcon(false)

  // const [menuActive, setMenuActive] = useState(false)
  // const [menuTriggered, setMenuTriggered] = useState(false)
  // const [scale, setScale] = useState(2)

  return (
    <>
      <Logo animationTrigger={props.animationTrigger} opacity={1} />
      <Masonry
        // width={masonryWidth}
        // height={masonryHeight}
        menuTriggered={props.menuTriggered}
        menuActive={props.menuActive}
        pType={props.pType}
        scale={props.scale}
        animationTrigger={props.animationTrigger}
        setPType={props.setPType}
        allowSetPType={props.allowSetPType}
        setAllowSetPType={props.setAllowSetPType}
      />
      <Logo animationTrigger={props.animationTrigger} opacity={0.4} />
    </>
  );
})
