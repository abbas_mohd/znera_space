import React from "react";
import { useSpring, animated } from "react-spring";
import { easePolyInOut } from "d3-ease";

function randomIntFromInterval(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

const rand = {
  0: () => randomIntFromInterval(2000, 2750),
  1: () => randomIntFromInterval(2750, 3500),
  2: () => randomIntFromInterval(3500, 4250),
  3: () => randomIntFromInterval(4250, 5000),
};

const commonConfig = {
  frequency: 0.5,
};

const Image = ({
  key,
  index,
  photo,
  left,
  top,
  margin,
  direction,
  mousePos,
  drag,
  scrollOffsetX,
  scrollOffsetY,
  menuActive,
  pType,
  imageAnimationState,
  filterByType,
}) => {
  // console.log("Rendering Image")
  // const galleryContainer = document.getElementById("masonry");
  // const firstRender = JSON.parse(document.getElementById("first-render").textContent);
  // const rand = (firstRender) ? Math.floor(Math.random() * 3500) + 1500 : Math.floor(Math.random() * 100);
  //Handle mouse hover animation
  // const [parallaxProps, setParallaxProps] = useSpring(() => ({ xy: [0, 0], config: { mass: 10, tension: 550, friction: 140 } }))
  // const parallaxTrans = (x, y) => `translate3d(${-(x / 10)}px,${-(y / 10)}px,0)`
  // const calc = (x, y) => ({ xy: [x - window.innerWidth / 2, y - window.innerHeight / 2]})
  // if (!drag && !menuActive) {
  //   setParallaxProps(calc(...mousePos.xy))
  // }
  // else if (menuActive) {
  //   setParallaxProps(calc([window]))
  // }

  // if (galleryContainer.offsetWidth > window.innerWidth) {
  //   var calcLeft = scrollOffsetX + left - (galleryContainer.offsetWidth - window.innerWidth) / 2;
  // } else {
  //   var calcLeft = scrollOffsetX + left + (window.innerWidth - galleryContainer.offsetWidth) / 2;
  // }

  // if (galleryContainer.offsetHeight > window.innerHeight) {
  //   var calcTop = scrollOffsetY + top - (galleryContainer.offsetHeight - window.innerHeight) / 2;
  // } else {
  //   var calcTop = scrollOffsetY + top + (window.innerHeight - galleryContainer.offsetHeight) / 2;
  // }

  // const originX = window.innerWidth / 2 - calcLeft;
  // const originY = window.innerHeight / 2 - calcTop;
  const originX = window.innerWidth / 2 - left;
  const originY = window.innerHeight / 2 - top;
  const calcRand = rand[index % 4]();

  // console.log(imageAnimationState)

  // console.log(filterByType);

  const divProps = {
    left: left,
    top: filterByType === null ? top : top + 267,
    width: photo.width,
    height: photo.height,
    immediate: (imageAnimationState == "reset") ? true : false,
  };

  const opacityRender = {
    firstRender: {
      delay: calcRand - 50,
      opacity: 1,
      grayscale: 0,
      from: {
        opacity: 0,
      },
      config: {
        frequency: 2.5,
      },
    },
    typeHover: {
      delay: 0,
      opacity: pType === null || pType == photo.type ? 1 : 0.5,
      grayscale: pType === null || pType == photo.type ? 0 : 1,
      config: {
        frequency: 0.5,
      },
    },
    typeSelect: {
      delay: 0,
      grayscale: 0,
      opacity: pType == photo.type ? 1 : 0,
      config: {
        frequency: 0.5,
      },
    },
    reset: {
      opacity: 1,
      grayscale: 0,
      immediate: true
    },
  };

  const scaleRender = {
    firstRender: {
      delay: calcRand - 1000,
      transform: "scale(1)",
      from: {
        transform: "scale(0.5)",
      },
      config: {
        duration: 7000 - calcRand,
        easing: easePolyInOut.exponent(5),
      },
    },
    typeSelect: {
      delay: 0,
      transform: pType == photo.type ? "scale(1)" : "scale(0.9)",
      config: {
        frequency: 0.5,
      },
    },
    reset: {
      transform: "scale(1)",
      immediate: true
    },
  };

  const opacitySpring = useSpring(opacityRender[imageAnimationState]);

  const animationStyle = {
    transformOrigin: `${originX}px ${originY}px`,
    margin: margin,
    display: "block",
    position: "absolute",
    filter: opacitySpring.grayscale.to((x) => `grayscale(${x})`),
    ...opacitySpring,
    ...useSpring(scaleRender[imageAnimationState]),
    ...useSpring(divProps),
  };

  return (
    <animated.div style={animationStyle}>
      <div
      // className={
      //   pType === null || pType == photo.type
      //     ? "img-container"
      //     : "img-container defocused"
      // }
      >
        <img alt={photo.title} className="img-fluid" src={photo.src} />
      </div>
      {/* </animated.div> */}
    </animated.div>
  );
};

export default React.memo(Image);
