import React, { useState, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useSpring, animated, config } from "react-spring";
import { easePolyOut } from "d3-ease";
import Gallery from "react-photo-gallery";
import Image from "./Image";

const useStyles = makeStyles((theme) => ({
  masonry: {
    cursor: "grab",
    position: "relative",
    height: "100vh",
    width: "100vw",
    "&:active": {
      cursor: "grabbing",
    },
  },
}));
const photos = JSON.parse(document.getElementById("p_imgs").textContent);

export default React.memo(function Masonry(props) {
  // console.log(`Rendering Masonry`)
  const [filterByType, setFilterByType] = useState(null);
  const filteredPhotos = (type) =>
    type === null ? photos : photos.filter((photo) => photo.type == type);
  const classes = useStyles();
  const [drag, setDrag] = useState(false);
  const scrollOffsetLimitX = window.innerWidth / 4;
  const scrollOffsetLimitY = window.innerHeight / 4;
  const [scrollOffsetX, setScrollOffsetX] = useState(0);
  const [scrollOffsetY, setScrollOffsetY] = useState(0);
  var previousTouch = null;
  const [mousePos, setMousePos] = useState(() => ({ xy: [0, 0] }));
  const [parallaxProps, setParallaxProps] = useSpring(() => ({
    xy: [0, 0],
    config: { mass: 10, tension: 550, friction: 140 },
  }));
  const dragStyle = useSpring({
    // transform: `translate3d(${props.animationTrigger=="scaleIn" ? scrollOffsetX : 0}px,${props.animationTrigger=="scaleIn" ? scrollOffsetY : 0}px,0)`,
    transform: ["scaleIn", "fadeIn", "firstRender"].includes(
      props.animationTrigger
    )
      ? `translate3d(${scrollOffsetX}px,${scrollOffsetY}px,0)`
      : `translate3d(0px,0px,0)`,
  });
  const parallaxTrans = (x, y) =>
    `translate3d(${-(x / 50)}px,${-(y / 50)}px,0)`;
  const calc = (x, y) => ({
    xy: [x - window.innerWidth / 2, y - window.innerHeight / 2],
  });
  const [imageAnimationState, setImageAnimationState] = useState("firstRender");

  function handleMouseMove(e) {
    if (!props.menuActive) {
      if (drag) {
        if (
          -scrollOffsetLimitX < scrollOffsetX &&
          scrollOffsetX < scrollOffsetLimitX
        ) {
          setScrollOffsetX(scrollOffsetX + e.movementX / 2);
        } else {
          if (-scrollOffsetLimitX >= scrollOffsetX)
            setScrollOffsetX(-scrollOffsetLimitX + 1);
          if (scrollOffsetLimitX <= scrollOffsetX)
            setScrollOffsetX(scrollOffsetLimitX - 1);
        }
        if (
          -scrollOffsetLimitY < scrollOffsetY &&
          scrollOffsetY < scrollOffsetLimitY
        ) {
          setScrollOffsetY(scrollOffsetY + e.movementY / 2);
        } else {
          if (-scrollOffsetLimitY >= scrollOffsetY)
            setScrollOffsetY(-scrollOffsetLimitY + 1);
          if (scrollOffsetLimitY <= scrollOffsetY)
            setScrollOffsetY(scrollOffsetLimitY - 1);
        }
      } else {
        setParallaxProps(calc(...mousePos.xy));
        setMousePos({ xy: [e.clientX, e.clientY] });
      }
    }
  }

  function handleTouchMove(e) {
    const touch = e.touches[0];

    if (previousTouch) {
      // be aware that these only store the movement of the first touch in the touches array
      e.movementX = touch.pageX - previousTouch.pageX;
      e.movementY = touch.pageY - previousTouch.pageY;
      if (
        -scrollOffsetLimitX < scrollOffsetX &&
        scrollOffsetX < scrollOffsetLimitX
      ) {
        setScrollOffsetX(scrollOffsetX + e.movementX * 2);
      } else {
        if (-scrollOffsetLimitX >= scrollOffsetX)
          setScrollOffsetX(-scrollOffsetLimitX + 1);
        if (scrollOffsetLimitX <= scrollOffsetX)
          setScrollOffsetX(scrollOffsetLimitX - 1);
      }
      if (
        -scrollOffsetLimitY < scrollOffsetY &&
        scrollOffsetY < scrollOffsetLimitY
      ) {
        setScrollOffsetY(scrollOffsetY + e.movementY * 2);
      } else {
        if (-scrollOffsetLimitY >= scrollOffsetY)
          setScrollOffsetY(-scrollOffsetLimitY + 1);
        if (scrollOffsetLimitY <= scrollOffsetY)
          setScrollOffsetY(scrollOffsetLimitY - 1);
      }
    }

    previousTouch = touch;
  }

  const imageRenderer = useCallback(
    ({ index, left, top, key, photo, margin, direction }) => (
      <Image
        key={key}
        index={index}
        photo={photo}
        left={left}
        top={top}
        margin={margin}
        direction={direction}
        pType={props.pType}
        imageAnimationState={imageAnimationState}
        filterByType={filterByType}
      />
    ),
    [props.pType, imageAnimationState, filterByType]
  );

  // const [animationSpring, api] = useSpring(() => ({
  //   transform: `translate(-50%, -50%) scale(${props.scale})`,
  //   config: {
  //     ...config.molasses,
  //     frequency: props.menuTriggered ? 0.5 : 7,
  //     easing: easePolyOut.exponent(0.5),
  //   },
  //   delay: props.menuTriggered ? 0 : 2000,
  //   from: {
  //     transform: `translate(-50%, -50%) scale(1)`,
  //   },
  // }))

  // switch (props.animationTrigger) {
  //   case "fadeIn":
  //     api.start({
  //       transform: `translate(-50%, -50%) scale(${props.scale})`,
  //       config: {
  //         ...config.molasses,
  //         frequency: props.menuTriggered ? 0.5 : 7,
  //         easing: easePolyOut.exponent(0.5),
  //       },
  //       delay: props.menuTriggered ? 0 : 2000,
  //       from: {
  //         transform: `translate(-50%, -50%) scale(1)`,
  //       },
  //     })
  //     break;
  //   case "fadeOut":
  //     api.start({
  //       opacity: 0,
  //       transform: `translate(-50%, -50%) scale(0.8)`,
  //       config: {
  //         ...config.molasses,
  //         frequency: 1,
  //       },
  //       onRest: () => {
  //         props.setPType(null);
  //         setScrollOffsetX(0);
  //         setScrollOffsetY(0);
  //       },
  //     })
  //     break;
  //   case "scaleOut":
  //     api.start({
  //       opacity: 1,
  //       transform: `translate(-50%, -50%) scale(1)`,
  //       config: {
  //         ...config.molasses,
  //         frequency: 0.5,
  //       }
  //     })
  //     break;
  //   case "scaleIn":
  //     api.start({
  //       opacity: 1,
  //       transform: `translate(-50%, -50%) scale(${props.scale})`,
  //       config: {
  //         ...config.molasses,
  //         frequency: 0.5,
  //       },
  //       // onRest: () => {
  //       //   props.setPType(null);
  //       // },
  //     })
  //     break;
  // }

  const animationStyle = {
    firstRender: () => {
      return {
        transform: `translate(-50%, -50%) scale(${props.scale})`,
        config: {
          ...config.molasses,
          frequency: props.menuTriggered ? 0.5 : 7,
          easing: easePolyOut.exponent(0.5),
        },
        delay: props.menuTriggered ? 0 : 2000,
        from: {
          transform: `translate(-50%, -50%) scale(1)`,
        },
      };
    },
    fadeIn: () => {
      if (filterByType !==null)
        setFilterByType(null);
      if (imageAnimationState != "reset")
        setImageAnimationState("reset");
      if (props.pType !== null) 
        props.setPType(null);
      return {
        opacity: 1,
        transform: `translate(-50%, -50%) scale(2)`,
        config: {
          ...config.molasses,
          frequency: 1,
        },
      };
    },
    fadeOut: () => {
      return {
        opacity: 0,
        transform: `translate(-50%, -50%) scale(0.8)`,
        config: {
          ...config.molasses,
          frequency: 1,
        },
        onRest: () => {
          props.setPType(null);
          setScrollOffsetX(0);
          setScrollOffsetY(0);
        },
      };
    },
    customFadeOut: () => {
      if (imageAnimationState != "typeSelect")
        setImageAnimationState("typeSelect");
      if (props.allowSetPType)
        props.setAllowSetPType(false);
      setTimeout(() => {
        setFilterByType(props.pType);
      }, 500);
      return {
        opacity: 0,
        delay: 1000,
        immediate: true
      };
      // if (props.pType !== null) props.setPType(null);
    },
    scaleOut: () => {
      if (imageAnimationState != "typeHover")
        setImageAnimationState("typeHover");
      if (props.allowSetPType === false)
        props.setAllowSetPType(true);
      return {
        opacity: 1,
        transform: `translate(-50%, -50%) scale(1)`,
        config: {
          ...config.molasses,
          frequency: 0.5,
        },
        // onRest: () => {
        //   setImageAnimationState("typeHover");
        // },
      };
    },
    scaleIn: () => {
      if (props.pType !== null) props.setPType(null);
      return {
        opacity: 1,
        transform: `translate(-50%, -50%) scale(${props.scale})`,
        config: {
          ...config.molasses,
          frequency: 0.5,
        },
        // onRest: () => {
        //   props.setPType(null);
        // },
      };
    },
  };
  const animationSpring = useSpring(animationStyle[props.animationTrigger]());

  const getMasonryColumns = () => {
    if (filterByType === null) {
      if (window.innerWidth > window.innerHeight) return 7;
      else return 4;
    } else return 3;
  };

  return (
    <div
      style={{ display: "flex", position: "fixed", top: "50vh", left: "50vw" }}
    >
      <animated.div
        // id="masonry"
        className={classes.masonry}
        style={animationSpring}
        //   style={{ transform: "scale(1)" }}
        onMouseDown={() => setDrag(true)}
        onMouseUp={() => setDrag(false)}
        onMouseMove={handleMouseMove}
        // onTouchStart ={() => setDrag(true)}
        onTouchEnd={() => (previousTouch = null)}
        onTouchMove={handleTouchMove}
      >
        <animated.div style={dragStyle}>
          <animated.div
            style={{ transform: parallaxProps.xy.to(parallaxTrans) }}
          >
            <Gallery
              photos={filteredPhotos(filterByType)}
              direction={"column"}
              columns={getMasonryColumns()}
              margin={window.innerWidth > window.innerHeight ? 20 : 10}
              renderImage={imageRenderer}
            />
          </animated.div>
        </animated.div>
      </animated.div>
    </div>
  );
});
