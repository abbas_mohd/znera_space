import React, { useRef, useLayoutEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import { useSpring, animated, config } from "react-spring";
import { Parallax, ParallaxLayer } from "@react-spring/parallax";

const photos = JSON.parse(document.getElementById("p_imgs").textContent);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    paddingTop: 267,
  },
  paper: {
    // padding: theme.spacing(3),
    padding: "0 1.16279vw 8.13953vw",
  },
  imgContainer: {
    overflow: "hidden",
    "&:hover": {
      "& img": {
        transform: "scale(1.03)",
      },
    },
    "& img": {
      transition: "transform 0.5s",
    },
  },
  imgFluid: {
    maxWidth: "100%",
    height: "auto",
  }
}));

// document.getElementsByClassName("no-scroll")[0].style.overflow = "auto";


export default React.memo(function ProjectDisplay(props) {
  props.setShowCloseIcon(true);
  props.setMenuActive(false);
  props.setAnimationTrigger("customFadeOut");
  props.serPageTransitionStyle("immediate");
  const classes = useStyles();
  
  const targetRef = useRef();
  
  // const [dimensions, setDimensions] = useState({ width: 0, height: 0 });
  
  // useLayoutEffect(() => {
  //   if (targetRef.current) {
  //     setDimensions({
  //       width: targetRef.current.offsetWidth,
  //       height: targetRef.current.offsetHeight,
  //     });
  //   }
  // }, []);
  
  if (props.currProject) {
    var filteredPhotos = photos.filter((x) => x.type == props.currProject);
  } else {
    var filteredPhotos = photos;
  }
  // batch = filteredPhotos.slice(0,3)
  // Math.max(...batch.map(a => a.height))
  
  const [scrollPosition, setScrollPosition] = useState(0);
  
  const scrollStyle = useSpring({
    transform: `translate3d(0px,${scrollPosition}px,0)`,
    config: config.slow,
  });

  const handleOnWheel = (e) => {
    const newPos = scrollPosition - e.deltaY;
    if (newPos >= 0) {
      setScrollPosition(0);
    } else if (newPos <= -(document.body.scrollHeight - window.innerHeight)) {
      // } else if (document.body.scrollHeight == window.innerHeight) {
      // setScrollPosition(-(document.body.scrollHeight - scrollPosition))
      setScrollPosition(
        e.deltaY > 0 ? scrollPosition : scrollPosition - e.deltaY
      );
    } else {
      setScrollPosition(scrollPosition - e.deltaY);
    }
  };

  // return (
  //   <Parallax
  //     pages={Math.trunc(filteredPhotos.length / 3)}
  //     style={{ top: "0", left: "0" }}
  //   >
  //     {filteredPhotos.map((photo, index) => (
  //       <ParallaxLayer
  //         offset={Math.trunc(index / 3) + 0.3}
  //         speed={0.5}
  //         style={{
  //           left: `${33 * (index % 3)}vw`,
  //         }}
  //       >
  //         <Paper className={classes.paper} elevation={0}>
  //           <div className={classes.imgContainer}>
  //             <img alt={photo.title} className={classes.imgFluid} src={photo.src} />
  //           </div>
  //           <div className="p-title">{photo.name}</div>
  //         </Paper>
  //       </ParallaxLayer>
  //     ))}
  //   </Parallax>
  // );

  return (
    <animated.div
      ref={targetRef}
      className={classes.root}
      style={scrollStyle}
      onWheel={handleOnWheel}
    >
      <Grid container spacing={0}>
        {filteredPhotos.map((photo, index) => (
          <Grid item xs={12} sm={6} lg={4}>
            <Paper className={classes.paper} elevation={0}>
              <div>
                <div className={classes.imgContainer}>
                  <img alt={photo.title} className="img-fluid" src={photo.src} />
                </div>
                <div className="p-title">{photo.name}</div>
              </div>
            </Paper>
          </Grid>
        ))}
      </Grid>
    </animated.div>
  );

  // return (
  //   <div className={classes.root} style={{width: "100vw"}}>
  //     <GridList cellHeight={160} className={classes.gridList} cols={3}>
  //       {photos.map((tile) => (
  //         <GridListTile key={tile.src} cols={tile.cols || 1}>
  //           <img src={tile.src} alt={tile.title} />
  //         </GridListTile>
  //       ))}
  //     </GridList>
  //   </div>
  // );
});
