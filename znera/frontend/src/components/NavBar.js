import React, { useState } from "react";
import { useSpring, animated } from "react-spring";
import ProjectDropdown from "./ProjectDropdown";
import { Link } from "react-router-dom"

export default React.memo(function NavBar(props) {
  // console.log(`Rendering NavBar`)
  const divStyle = useSpring({
    opacity: 1,
    delay: 1,
    from: {
      opacity: 0,
    },
    config: {
      frequency: 1.5,
    },
  });

  function handleWorkOnClick(e) {
    props.setMenuTriggered(true);
    if (props.menuActive) {
      //set Masonry back to it's previous state
      // setMasonryWidth(200)
      // setMasonryHeight(200)
      // props.setScale(2);
      props.setMenuActive(false);
      props.setAnimationTrigger("scaleIn");
    } else {
      //Make masonry fit viewport when menu is active
      // setMasonryWidth(100)
      // setMasonryHeight(100)
      // props.setScale(1);
      props.setMenuActive(true);
      props.setAnimationTrigger("scaleOut");
    }
  }

  return (
    <>
      <animated.nav className="nav" style={divStyle}>
        <Link to={`/about`}>
          <span className="nav-link">
            About
          </span>
        </Link>
        <div className="nav-divider" />
        <span className="nav-link" onClick={handleWorkOnClick}>
          Work
        </span>
        <div className="nav-divider" />
        <span className="nav-link">
          Contact
        </span>
        <div className="nav-divider" />
        <span>Awards</span>
      </animated.nav>
      <ProjectDropdown
        menuActive={props.menuActive}
        setPType={props.setPType}
        allowSetPType={props.allowSetPType}
      />
    </>
  );
})
