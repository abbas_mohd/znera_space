import React from "react";
import { useSpring, useTransition, animated } from "react-spring";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    position: "fixed",
    top: "50vh",
    left: "50vw",
    pointerEvents: "none",
  },
  logo: {
    width: "40vw",
    [theme.breakpoints.up("md")]: {
      width: "25vw",
    },
  },
}));

export default React.memo(function Logo(props) {
  // console.log(`Rendering Logo ${props.opacity}`);
  const classes = useStyles();
  const props2 = useSpring({
    width: "100vw",
    height: 158,
    position: "fixed",
    background: "linear-gradient(to right, transparent 100%, white 100%)",
    transform: "translate(-50%, -50%)",
    from: {
      background: "linear-gradient(to right, transparent 0%, white 10%)",
    },
    config: {
      duration: 3000,
    },
  });
  const animationStyle = {
    firstRender: {
      transform: "translate(-50%, -50%) scale(1.7)",
      opacity: props.opacity,
      from: {
        transform: "translate(-50%, -50%) scale(1)",
        opacity: props.opacity,
      },
      config: {
        frequency: 5,
      },
    },
    fadeIn: {
      opacity: props.opacity,
      config: {
        frequency: 1,
      },
    },
    fadeOut: {
      opacity: 0,
      config: {
        frequency: 1,
      },
    },
    customFadeOut: {
      opacity: 0,
      config: {
        frequency: 1,
      },
    },
  };
  const props1 = useSpring(animationStyle[props.animationTrigger]);
  return (
    <div className={classes.root}>
      {/* {props1(
        (styles, item) =>
          item && ( */}
      <animated.div style={props1}>
        <img
          src="/static/images/logo.svg"
          alt="ZN  I Era"
          class={classes.logo}
        ></img>
      </animated.div>
      {/* )
      )} */}
      <animated.div style={props2} />
    </div>
  );
})
