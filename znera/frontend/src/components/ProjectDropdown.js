import React, { useState } from "react";
import { useSpring, useTransition, useTrail, animated } from "react-spring";
import { Link } from "react-router-dom"

const menuItems = JSON.parse(document.getElementById("p_types").textContent);
export default function ProjectDropdown(props) {
  // console.log(`Rendering ProjectDropdown`);
  

  const transitions = useTransition(props.menuActive, {
    from: {
      opacity: 0,
      height: 0,
      // transform: 'translateY(-10%)',
    },
    enter: {
      opacity: 1,
      height: 'auto',
      // transform: 'translate(0%)',
    },
    leave: { opacity: 0 },
    // expires: () => props.setPType(null)
  })

  // const handleMouseEnter = (index) => props.setPType(menuItems[index].id)
  // const handleMouseLeave = () => props.setPType(null)
  const handleMouseEnter = (e) => {
    if(props.allowSetPType)
      props.setPType(e.target.dataset["type"])
  }
  const handleMouseLeave = (e) => {
    if(props.allowSetPType)
      props.setPType(null)
  }

  const trail = useTrail(menuItems.length, { from: { opacity: 0},  to:{ opacity: 1 } })

  return (
      <div>
        {transitions((styles, item) => item && (
          <animated.div
            style={styles}
            className="card"
          >
            <div className="menu">
              {trail.map((props, index) => (
                <nav>
                  <ul>
                    <animated.li className="menu-item" style={props}>
                      <Link to={`/projects/${menuItems[index].slug}`}>
                        <div className="menu-link" data-type={menuItems[index].id} onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
                          {menuItems[index].name}<sup className="menu-item-sup">{menuItems[index].count}</sup>
                        </div>
                      </Link>
                    </animated.li>
                  </ul>
                </nav>
              ))}
            </div>
          </animated.div>
        ))}
      </div>
    
  );
}
