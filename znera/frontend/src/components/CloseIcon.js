import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { useTransition, animated, config } from "react-spring";

const useStyles = makeStyles((theme) => ({
  _3AwJwE: {
    position: "fixed",
    top: "2.32558vw",
    right: "1.16279vw",
    width: "48px",
    height: "48px",
    zIndex: 17,
    // -webkit-transition: opacity .35s cubic-bezier(.86,0,.07,1),-webkit-transform .7s cubic-bezier(.19,1,.22,1),
    transition:
      "transform .7s cubic-bezier(.19,1,.22,1),opacity .35s cubic-bezier(.86,0,.07,1),-webkit-transform .7s cubic-bezier(.19,1,.22,1)",
  },
  _3So2RN: {
    borderRadius: "50%",
    display: "block",
    width: "48px",
    height: "48px",
    position: "relative",
    overflow: "visible",
    cursor: "pointer",
    "&:after": {
      borderRadius: "50%",
      display: "block",
      content: '""',
      width: "100%",
      height: "100%",
      background: "#f2f2f2",
      // -webkit-transition: background .75s cubic-bezier(.19,1,.22,1),
      transition: "background .75s cubic-bezier(.19,1,.22,1)",
    },
  },
  _2h4A5y: {
    position: "absolute",
    top: "50%",
    left: "50%",
    // -webkit-transform: "translate(-50%,-50%)",
    transform: "translate(-50%,-50%)",
    display: "inline-block",
    width: "10px",
    height: "10px",
    "&:before": {
      content: '""',
      display: "block",
      width: "140%",
      position: "absolute",
      top: "50%",
      left: "50%",
      // -webkit-transform: translate(-50%,-50%) rotate(-45deg),
      transform: "translate(-50%,-50%) rotate(-45deg)",
      height: "2px",
      background: "#000",
    },
    "&:after": {
      content: '""',
      display: "block",
      width: "2px",
      position: "absolute",
      top: "50%",
      left: "50%",
      // -webkit-transform: translate(-50%,-50%) rotate(-45deg),
      transform: "translate(-50%,-50%) rotate(-45deg)",
      height: "140%",
      background: "#000",
    },
  },
  _3y5exE: {
    width: "100%",
    height: "100%",
    position: "absolute",
    top: 0,
    left: 0,
    opacity: 0,
    // -webkit-transition: opacity .45s cubic-bezier(.215,.61,.355,1),
    transition: "opacity .45s cubic-bezier(.215,.61,.355,1)",
    pointerEvents: "none",
  },
}));

export default React.memo(function CloseIcon(props) {
  // console.log(`rendering CloseIcon`);
  const classes = useStyles();
  // props.setAnimationTrigger("fadeIn");
  // props.setScale(2);

  // const show = true

  const transitions = useTransition(props.show, {
    from: {
      opacity: 0,
      transform: "rotate(-70deg)",
    },
    enter: {
      opacity: 1,
      transform: "rotate(0deg)",
    },
    leave: {
      opacity: 0,
      transform: "rotate(-70deg)",
    },
    config: config.molasses,
  });

  return (
    <Link to="/">
      <div class={classes._3AwJwE} onClick={props.handleClick}>
        {transitions(
          (styles, item) =>
            item && (
              <animated.div style={styles} class={classes._3So2RN}>
                <div class={classes._2h4A5y}></div>
                <div class={classes._3y5exE}>
                  <svg
                    viewBox="0 0 100 100"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    class="_3u7D7A"
                  >
                    <circle
                      cx="50"
                      cy="50"
                      r="48"
                      stroke="black"
                      stroke-width="4"
                      style={{ strokeDashoffset: 301.1 }}
                    ></circle>
                  </svg>
                </div>
              </animated.div>
            )
        )}
      </div>
    </Link>
  );
})
