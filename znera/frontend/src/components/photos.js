export const photos = [
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1605429771150-GJQEHTC475RDCBL61DSW/ke17ZwdGBToddI8pDm48kPJXHKy2-mnvrsdpGQjlhod7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QHyNOqBUUEtDDsRWrJLTmihaE5rlzFBImxTetd_yW5btdZx37rH5fuWDtePBPDaHF5LxdCVHkNEqSYPsUQCdT/jal%2B3.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1605017331029-UV2GHNHOYRSX9Z2A4OQK/ke17ZwdGBToddI8pDm48kCMoFfAcaHogH5iBLAJikgR7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0hZPx-jNbZA_TaS-5l2nNKF7ASycDZJDjDsF5R4WAHUa-_BV0jUXSO9vOJFSbtS2mw/Dual+Villa_Camera+2+R1.jpg',
      width: 1000,
      height: 746
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1605017028440-5G1U1RUX7D2QQCRUIY8J/ke17ZwdGBToddI8pDm48kH2mgFeGpBBPjyOun4kQpDJ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QHyNOqBUUEtDDsRWrJLTmnhbJtrfwp8bfXu9iW6Tjn3nYhG4Rw1_C-wnAJ2S5wYd1jCdpmp9dwPFYGptjfKZX/image.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604833406334-V6WS0B08UXT6F4TBZCKD/ke17ZwdGBToddI8pDm48kOyctPanBqSdf7WQMpY1FsRZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpwwQIrqN0bcqL_6-iJCOAA0qwytzcs0JTq1XS2aqVbyK6GtMIM7F0DGeOwCXa63_4k/unspecified%2B%25289%2529.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604578967220-N7JA8L8X332Y9N4QL2ZP/ke17ZwdGBToddI8pDm48kFNDRUyZr6Y7L5rLVMQlUi97gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1Ufhbi2iyzffI_NFhLtMsrANDvdu7xtMd-R5D4ejPFoyZmERIekx847MRJ7GDQL_aAw/V1_N_1.jpg',
      width: 600,
      height: 600
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604574685460-AG1I6KCFM9Y1RP4VCB5J/ke17ZwdGBToddI8pDm48kMRdtqsL-ZqrIEahCUnrX8d7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UfUrD0Pf5U58Q7AuW6D6VqwIUr7QtgY5D8fYAdDe2UuARY5byGCXiM996O21yNVyKA/image.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604574420861-V89P466UL6HN4AAGEKFJ/ke17ZwdGBToddI8pDm48kFsEwUsnGWxzeFgNQ1pqZ_l7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UQA5WU-ypBsnHLQrZvZ94yhreDFSMP0iGvNJcgOI7i736xZmbWWZLidMHit5X5DFIg/image.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604574132186-8FRFUK13U00YVO6SMUDN/ke17ZwdGBToddI8pDm48kOvj-jDXXOKY_Lvbkt5eHCt7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UVGFVGbuw_rNEpzHgzEHyMb3qvd_KWQ-vd01rx_N5OiYqN0oR6KEvo4CBbjgVS6eJg/image.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604566975274-TELVQA5QW09BUD82PCF6/ke17ZwdGBToddI8pDm48kOKLA4nWpu_7gGxQDQqnHdp7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QHyNOqBUUEtDDsRWrJLTmyh-8_5GJNvrfz4o4yOfLSyHGVVZJeCSdaZrJmaJfB8tVsgs5eGZSZFqbsy-olSCh/image.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604566810217-Y1PFJBLGNHELABARHH9R/ke17ZwdGBToddI8pDm48kOT3TvJfIIC1uVbg_2SJ9Q97gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UYW4vSDEqsNN7DUvIw2NXME-wKYEdovW_LE_sR0zzYseeYCtFRjqD8waZe01I-2sbQ/Usman_2%2B-%2BCopy.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604566155886-AQO73WO4IAW7M5SHV9JP/ke17ZwdGBToddI8pDm48kLinFFHfOWxXfI2MB8J8Z5F7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UeCxvKzvbr2aY_wkb4kfp5UNe_pYlreFWyc7yFYM3uBMtrU8UFk4BXyUXL-Dw_TrVg/image.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604566024089-85T3DZ94D1SSAPPNMXP5/ke17ZwdGBToddI8pDm48kCM5uveFmDR3IzhYSe6SQNh7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UUC_ltddeCHYwzsucT9VncWaf5Tf3w2HLCWAh-nNxnyahe7YIBkR7oWuXFY-eIdLGg/Eye%2BLevel-001%2Bfinal%2Bcopy.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604485573915-7N26HPZXVST0Q3R7LMXY/ke17ZwdGBToddI8pDm48kD6YbgPv1BSMVYSBa5WYk-d7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UZPf5JWmGneR4JCZPBDMofrIaZDEXJDVKf1t1PME0Ui8hMNhSClprbddOHo9kllY8A/aq13.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604564573384-EC2M1GMUOX7VD6BSH5K9/ke17ZwdGBToddI8pDm48kMRyhYDgPk9y7GMUxyVGe-RZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpzEOKU3ynhxGCOkjkiI2jkNnwo4GpKkY3nGgozguHn9FA2PHrUGCg52mo1A4uM7YTw/image.jpg',
      width: 720,
      height: 720
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604560350967-6XFB7WB15505GNBCAYR8/ke17ZwdGBToddI8pDm48kH2Byju3b2k1QW5WTENQ6mx7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1URsDrUpTKHtodQXaOmPLB-Z4uA_pQt56_TF9uGGpRhih5Pc19j1LVISwmFTcyujgXQ/TAJ_2.1.jpg',
      width: 1000,
      height: 996
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604560032040-EQUA4RQOXROEGL8V8J3V/ke17ZwdGBToddI8pDm48kD6YbgPv1BSMVYSBa5WYk-d7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UZPf5JWmGneR4JCZPBDMofrIaZDEXJDVKf1t1PME0Ui8hMNhSClprbddOHo9kllY8A/EyeView03.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604488901074-F9IYLZPOZ2NU6A5PX0DJ/ke17ZwdGBToddI8pDm48kNiEM88mrzHRsd1mQ3bxVct7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0s0XaMNjCqAzRibjnE_wBlkZ2axuMlPfqFLWy-3Tjp4nKScCHg1XF4aLsQJlo6oYbA/IMAGE%2B1.1.jpg',
      width: 1000,
      height: 1000
    },
    {
      src: 'https://images.squarespace-cdn.com/content/v1/58d77f7b3e00bed502847a45/1604832410281-WVEUN8NHCVEDKXBWVNJW/ke17ZwdGBToddI8pDm48kB3Q_QkPsZvsHXUEtIpaakt7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UTdkUBLoRM-8jw7DhQl2GSt5gnjBa_kdSXUMj7jJOUELmERIekx847MRJ7GDQL_aAw/7.jpg',
      width: 1000,
      height: 1000
    }
  ]
  