import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useSpring, animated, config } from "react-spring";

const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    width: "100vw",
    display: "flex",
    flexDirection: "column",
    background: "#fff",
    padding: "20vw 6.66667vw 13.33333vw",
    zIndex: 1,
    // -webkit-transition: background .45s ease,-webkit-transform .75s cubic-bezier(.19,1,.22,1),
    // transition: background .45s ease,-webkit-transform .75s cubic-bezier(.19,1,.22,1),
    // transition: transform .75s cubic-bezier(.19,1,.22,1),background .45s ease,
    // transition: transform .75s cubic-bezier(.19,1,.22,1),background .45s ease,-webkit-transform .75s cubic-bezier(.19,1,.22,1),
    // -webkit-transform: translateX(11.66667vw),
    // transform: translateX(11.66667vw),
    ["@media (min-width:1024px)"]: {
      padding: "16.27907vw 2.32558vw",
    },
  },
  title1: {
    fontSize: 42,
    fontSize: "2.71318vw",
    fontFamily: "Optima,Helvetica,Arial,sans-serif",
    fontWeight: 500,
    lineHeight: 1.05,
    letterSpacing: "-.05em",
    marginBottom: "20vw",
    ["@media (min-width: 1024px)"]: {
      fontSize: 96,
      lineHeight: 1.1,
      marginBottom: "13.95349vw",
    },
    ["@media (min-width: 768px)"]: {
      width: "75%",
    },
    ["@media (max-width: 1548px)"]: {
      fontSize: 42,
    },
  },
  paraCommon: {
    fontSize: 16,
    fontSize: "1.03359vw",
    fontFamily: "Graphik,Helvetica,Arial,sans-serif",
    fontWeight: "400",
    letterSpacing: "0",
    lineHeight: "1.4",
    ["@media (min-width: 1024px)"]: {
      fontSize: "28px",
      lineHeight: "1.35",
      letterSpacing: "-.03em",
    },
    ["@media (max-width: 1548px)"]: {
      fontSize: 16,
    },
  },
  para1: {
    paddingLeft: "0",
    marginBottom: "20vw",
    ["@media (min-width: 1024px)"]: {
      paddingRight: "2.32558vw",
      marginBottom: " 4.65116vw",
      marginLeft: "auto",
      width: "41.86047vw",
    },
  },
  imgContainer: {
    paddingBottom: "66%",
    position: "relative",
    width: "100%",
    height: "100%",
    overflow: "hidden",
  },
  img: {
    backgroundImage: "url('/static/images/about-1.jpg')",
    backgroundPosition: "50%",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
  },
  paraImgContainer: {
    marginBottom: "3.48837vw",
    marginTop: "9.30233vw",
    ["@media (min-width: 1024px)"]: {
      marginBottom: "13.95349vw",
      display: "grid",
      gridTemplateColumns: "repeat(2,1fr)",
      gridTemplateRows: "max-content auto",      
      marginTop: "15.11628vw",
    },
    ["@media (min-width: 768px)"]: {
      marginBottom: "3.48837vw",
      marginTop: "9.30233vw",
    },
  },
  para2: {
    ["@media (min-width: 1024px)"]: {
      marginBottom: "2.32558vw",
      marginRight: "6.97674vw",
    },
  },
}));

export default function AboutUs(props) {
  const classes = useStyles();
  props.setShowCloseIcon(true);
  props.setMenuActive(false);
  props.setAnimationTrigger("fadeOut");
  props.serPageTransitionStyle("fadeIn");
  // props.setShowNav=false

  const [scrollPosition, setScrollPosition] = useState(0);

  const scrollStyle = useSpring({
    transform: `translate3d(0px,${scrollPosition}px,0)`,
    config: config.slow,
  });

  const handleOnWheel = (e) => {
    const newPos = scrollPosition - e.deltaY;
    if (newPos >= 0) {
      setScrollPosition(0);
    } else if (newPos <= -(document.body.scrollHeight - window.innerHeight)) {
      // } else if (document.body.scrollHeight == window.innerHeight) {
      // setScrollPosition(-(document.body.scrollHeight - scrollPosition))
      setScrollPosition(
        e.deltaY > 0 ? scrollPosition : scrollPosition - e.deltaY
      );
    } else {
      setScrollPosition(scrollPosition - e.deltaY);
    }
  };

  return (
    <animated.div style={scrollStyle} onWheel={handleOnWheel}>
      <div className={classes.root}>
        <div className={classes.title1}>About ZN Era</div>
        <div className={`${classes.paraCommon} ${classes.para1}`}>
          ZN | Era The Dubai architectural office works around the globe
          creating places that offer special spatial experiences. The firm's
          repertoire ranges from spectacular cultural institutions and
          innovative residential and office buildings to exhibitions, furniture
          and product design | ZNera stands for excellence in implementation and
          delight in experimentation, for the awareness of people and context,
          for the crystalline and fluis, for realism and vision, for dynamic and
          intimate atmospheres, for practicality and passion. | As a result, the
          firm’s skepticism toward abstraction, the diagrammatic and
          generalizations, visa-vies their interest in detail, fabric, texture,
          the sensual quality of materials and surfaces and the individuals who
          exist in these environments is part of this growing architectural
          language.
        </div>
        <div className={classes.imgContainer}>
          <div className={classes.img}></div>
        </div>
        <div className={classes.paraImgContainer}>
          <div className={`${classes.paraCommon} ${classes.para2}`}>
            ZN | Era believes there is no coherent materiality or spatiality but
            rather a juxtaposition of various materials and different levels of
            representation. By moving in and around the work | we aim to
            simultaneously induce perceptions of discontinuity and synthesis |
            The construction as such enables us to simultaneously experience the
            duality of identity between the self and society
          </div>
        </div>
      </div>
    </animated.div>
  );
}
