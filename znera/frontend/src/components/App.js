import React, { useState, useCallback } from "react";
import { useSpring, useTransition, animated } from "react-spring";
import ReactDOM from "react-dom";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useLocation,
  // Link,
  // Redirect,
} from "react-router-dom";
// import shuffle from 'lodash/shuffle'
import Logo from "./Logo";
import Landing from "./Landing";
import ProjectDisplay from "./ProjectDisplay";
import CloseIcon from "./CloseIcon";
import NavBar from "./NavBar";
import AboutUs from "./AboutUs";

function App() {
  // const currProject = useRef(null)const { location } = useContext(__RouterContext);
  // let location = useLocation();
  // console.log(location);
  // const transitions = useTransition(location, {
  //   key: location.pathname,
  //   from: { opacity: 0 },
  //   enter: { opacity: 1 },
  //   leave: { opacity: 0 },
  // });

  const menuItems = JSON.parse(document.getElementById("p_types").textContent);
  const [showCloseIcon, setShowCloseIcon] = useState(false);

  const [menuActive, setMenuActive] = useState(false);
  const [menuTriggered, setMenuTriggered] = useState(false);
  // const [navTransition, setNavTransition] = useState("immediate")
  const [scale, setScale] = useState(2);
  const [pType, setPType] = useState(null);
  const [allowSetPType, setAllowSetPType] = useState(true);
  const [animationTrigger, setAnimationTrigger] = useState("firstRender");
  const [pageTransitionStyle, serPageTransitionStyle] = useState("fadeOut");

  const closeIcon = useCallback(() => {
    setShowCloseIcon(false);
    setAnimationTrigger("fadeIn");
    serPageTransitionStyle("fadeOut");
  }, [showCloseIcon]);

  const displayStyle = {
    fadeOut: () => {
      return {
        opacity: 0,
        config: {
          frequency: 1,
        },
      };
    },
    fadeIn: () => {
      return {
        opacity: 1,
        delay: 1000,
      };
    },
    immediate: () => {
      return {
        opacity: 1,
        delay: 1000,
        immediate: true,
      };
    },
  };

  const pageTransition = useSpring(displayStyle[pageTransitionStyle]());

  return (
    <div>
      <NavBar
        menuActive={menuActive}
        setMenuActive={setMenuActive}
        setMenuTriggered={setMenuTriggered}
        setScale={setScale}
        setPType={setPType}
        setAnimationTrigger={setAnimationTrigger}
        allowSetPType={allowSetPType}
      />
      <Landing
        menuActive={menuActive}
        menuTriggered={menuTriggered}
        scale={scale}
        setShowCloseIcon={setShowCloseIcon}
        pType={pType}
        animationTrigger={animationTrigger}
        setPType={setPType}
        allowSetPType={allowSetPType}
        setAllowSetPType={setAllowSetPType}
      />
      <CloseIcon show={showCloseIcon} handleClick={closeIcon} />
      <Switch>
        <Route exact path="/"></Route>
        {/* <animated.div style={aboutDisplayAnimation}> */}
        <animated.div style={pageTransition}>
          <Route exact path="/about">
            <AboutUs
              setShowCloseIcon={setShowCloseIcon}
              setMenuActive={setMenuActive}
              setAnimationTrigger={setAnimationTrigger}
              serPageTransitionStyle={serPageTransitionStyle}
            />
          </Route>
          {/* </animated.div> */}
          {menuItems.map((item, index) => (
            <Route exact path={`/projects/${item.slug}`}>
              <ProjectDisplay
                setShowCloseIcon={setShowCloseIcon}
                setMenuActive={setMenuActive}
                setAnimationTrigger={setAnimationTrigger}
                currProject={item.id}
                serPageTransitionStyle={serPageTransitionStyle}
              />
            </Route>
          ))}
        </animated.div>
      </Switch>
    </div>
  );
}

ReactDOM.render(
  <Router>
    <App />
  </Router>,
  document.getElementById("main")
);
