from django.shortcuts import render
# from django.core import serializers
from django.db.models import Count
from django.utils.text import slugify
from frontend.models import ProjectImage, ProjectType

# Create your views here.
def index (request, *args, **kwargs):
    # pro_imgs_qs = ProjectImage.objects.filter(default=True)
    # project_images = [{
    #         'src': pro_img_obj.image.url,
    #         'width': pro_img_obj.image_width,
    #         'height': pro_img_obj.image_height,
    # } for pro_img_obj in ProjectImage.objects.filter(default=True)]
    project_images = ProjectImage.objects.select_related('project__project_type').filter(default=True)
    context = {
        'project_images': project_images,
        'project_types': [{'id': p_type.pk, 'name': p_type.name,'slug': slugify(p_type.name), 'count': p_type.p_count} for p_type in ProjectType.objects.annotate(p_count=Count('project')).filter(p_count__gt=0)],
    }
    return render(request, 'frontend/index.html', context)
