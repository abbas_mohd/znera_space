FROM python:3-slim
ENV PYTHONUNBUFFERED=1
WORKDIR /znera
COPY requirements.txt /znera/
RUN pip install -r requirements.txt
COPY ./znera /znera/