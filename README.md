# README #

### How do I get set up? ###

#### Install Dependencies ####
1. Docker
2. NodeJS

#### Deployment ####
Open Powershell in the repoitory directory and run these commands

	> site-control.ps1 build
	> site-control.ps1 up
Once the docker is up and Running, the webiste is accesible on localhost:8000


#### `site-control.ps1` script commands ####

1. `build`:  builds the docker Image
2. `up`: brings the docker up
3. `down`: brings the docker down
4. `bash`: Open bash script inside the docker container
5. `logs`: Shows the running docker container logs
6. `shell`: Runs `python manage.py shell` command inside the docker container
