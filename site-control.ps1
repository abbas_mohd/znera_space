$help = "`n'$args' is not a site-control command
`nCommands:
`tbuild`tBuild or rebuild services
`tup`tCreate and start containers
`tdown`tStop and remove containers, networks, images, and volumes
`tbash`tStart bash inside the container
`tshell`tStart shell inside the container
`tlogs`tFetch the logs"
Switch($args[0]) {
    "build" {docker-compose build; Break}
    "up" {docker-compose up -d; Break}
    "down" {docker-compose down; Break}
    "logs" {
        "`nExit logs with CONTROL-C`n"
        docker logs znera_web -f --tail=100; 
        Break
    }
    "bash" {
        "`nExit bash with CONTROL-D`n"
        docker exec -it znera_web bash;
        Break
    }
    "shell" {
        "`nExit bash with CONTROL-D`n"
        docker exec -it znera_web python manage.py shell;
        Break
    }
    Default {
        $help;
        Break
    }
}